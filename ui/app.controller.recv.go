package main

import "encoding/hex"

func (a *AppReceive) getPayRequests(addr string) {
	var reqs = GetPayRequests(addr)
	for _, v := range reqs {
		if stealthrequests[addr] == nil {
			stealthrequests[addr] = make(map[[3]uint64]struct{})
		}
		stealthrequests[addr][[3]uint64{v.OptAmount, v.OptExpireAt, v.CreatedAt}] = struct{}{}
	}

	a.ViewRequests(addr)

}
func (a *AppReceive) getApiKey(stealthAddr string) string {

	var mainAddr = a.globalAddress
	var padAddr = a.findStealthStack(mainAddr, stealthAddr, 0)

	return mainAddr + padAddr + "0000000000000000"
}

func (a *AppReceive) findStealthStack(backingkey, address string, off uint64) string {
	if !AddrsCompatible(backingkey) {
		return ""
	}
	var key, err = hex.DecodeString(backingkey)
	if err != nil {
		return ""
	}
	var testnet = !isMainnetAddr(backingkey)

	var stack [72]byte
	copy(stack[0:32], key[0:32])
	stack[40] = byte(off >> 56)
	stack[39] = byte(off >> 48)
	stack[38] = byte(off >> 40)
	stack[37] = byte(off >> 32)
	stack[36] = byte(off >> 24)
	stack[35] = byte(off >> 16)
	stack[34] = byte(off >> 8)
	stack[33] = byte(off >> 0)

	for i := uint16(0); i < 256; i++ {

		stack[32] = byte(i)

		var addr = nethash(stack[0:], testnet)

		var combaddr = CombAddr(addr, testnet)

		if combaddr == address {
			var buf [32]byte
			copy(buf[:], stack[32:64])
			return CombAddr(buf, testnet)
		}

	}
	return ""
}

func (a *AppReceive) generateStealth(backingkey string, off uint64, check bool) {

	if !AddrsCompatible(backingkey) {
		return
	}

	stealthbalances = make(map[string]uint64)

	var key, err = hex.DecodeString(backingkey)
	if err != nil {
		return
	}
	var testnet = !isMainnetAddr(backingkey)

	var stack [72]byte
	copy(stack[0:32], key[0:32])
	stack[40] = byte(off >> 56)
	stack[39] = byte(off >> 48)
	stack[38] = byte(off >> 40)
	stack[37] = byte(off >> 32)
	stack[36] = byte(off >> 24)
	stack[35] = byte(off >> 16)
	stack[34] = byte(off >> 8)
	stack[33] = byte(off >> 0)

	for i := uint16(0); i < 256; i++ {

		stack[32] = byte(i)

		var addr = nethash(stack[0:], testnet)

		var combaddr = CombAddr(addr, testnet)

		if check {
			var buf [32]byte
			copy(buf[:], stack[32:64])
			stealthbalances[combaddr] = CheckBalance(combaddr, backingkey, CombAddr(buf, testnet))
		} else {
			stealthbalances[combaddr] = 0
		}

	}

	a.ViewStealth()

}
