package main

import "fmt"
import "time"

func (a *App) loadCss() {
	var link = jQuery("#link")
	link.Set("rel", "stylesheet")
	link.Set("href", "style.css")
	link.Set("type", "text/css")
}

func timeToStr(t uint64) string {
	tm := time.Unix(int64(t), 0)
	return tm.String()
}

func row(k string, v uint64) string {
	return "<td name='key'>" + k + "</td><td name='bal'>" + fmt.Sprintf("%d.%08d", combs(v), nats(v)) + "</td>"
}
func row3(a, b, c uint64) string {
	return "<td>" + fmt.Sprintf("%d.%08d", combs(a), nats(a)) + "</td><td>" + timeToStr(b) + "</td><td>" + timeToStr(c) + "</td>"
}
