package main

type App struct {
	loader JQuery
	menu   *AppMain
	main   *AppMain
	send   *AppSend
	recv   *AppReceive
}

func NewApp() *App {
	return &App{jQuery("#loader"),
		&AppMain{
			jQuery("#main-addr"),
			jQuery("#mainbutton-send"),
			jQuery("#mainbutton-recv"),
		},
		&AppMain{
			jQuery("#main-addr"),
			jQuery("#body-send"),
			jQuery("#body-recv"),
		},
		&AppSend{
			"",
			jQuery("#send-reqs"),
			jQuery("#send-history"),
			jQuery("#send-upload"),
		},
		&AppReceive{
			"",
			jQuery("#recv-stealths"),
			jQuery("#recv-stealths-paginator"),
			jQuery("#recv-key-clicked"),
			jQuery("#recv-image"),
			jQuery("#recv-image2"),
			jQuery("#recv-key"),
			jQuery("#recv-key-claim"),
			jQuery("#recv-request"),
			jQuery("#recv-amount"),
			jQuery("#recv-created"),
			jQuery("#recv-expires"),
			jQuery("#recv-reqs"),
			jQuery("#recv-download"),
		},
	}
}

type AppMain struct {
	addr JQuery
	send JQuery
	recv JQuery
}
type AppSend struct {
	globalAddress string

	requests JQuery
	history  JQuery
	upload   JQuery
}
type AppReceive struct {
	globalAddress string

	stealths          JQuery
	stealthspaginator JQuery
	keyclicked        JQuery
	image             JQuery
	image2            JQuery
	key               JQuery
	keyclaim          JQuery
	request           JQuery
	amount            JQuery
	created           JQuery
	expires           JQuery
	requests          JQuery
	download          JQuery
}
