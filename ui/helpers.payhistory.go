package main

import "encoding/json"

type PutPayHistory struct {
	Address   string
	History   string
	CreatedAt uint64
}

func PayHistory(req *PutPayHistory) bool {
	jsonBytes, err := json.Marshal(req)
	if err != nil {
		return false
	}

	jsonData, err := GetJsonData("/PayHistory.put", string(jsonBytes), "js")
	if err != nil {
		return false
	}
	jsonVal := Parse(jsonData)
	if jsonVal == nil {
		return false
	}
	return jsonVal.Success
}

func GetHistories(addr, apiKey string) (res map[string]struct{}) {
	req := AllRequest{Key: "Address", Value: addr, ApiKey: apiKey}
	jsonBytes, err := json.Marshal(req)
	if err != nil {
		return nil
	}

	jsonData, err := GetJsonData("/PayHistory.all", string(jsonBytes), "js")
	if err != nil {
		return nil
	}
	jsonVal := Parse(jsonData)
	res = make(map[string]struct{})
	for _, row := range jsonVal.Rows {
		val1, ok1 := row["History"].(string)
		if !ok1 {
			continue
		}
		res[val1] = struct{}{}
	}

	return res
}
