package main

import "encoding/json"

type PutPayRequest struct {
	ApiKey      string
	Address     string
	OptAmount   uint64
	CreatedAt   uint64
	OptExpireAt uint64
}

func PayRequest(req *PutPayRequest) bool {
	jsonBytes, err := json.Marshal(req)
	if err != nil {
		return false
	}

	jsonData, err := GetJsonData("/PayRequest.put", string(jsonBytes), "js")
	if err != nil {
		return false
	}
	jsonVal := Parse(jsonData)
	if jsonVal == nil {
		return false
	}
	return jsonVal.Success
}

func GetPayRequests(addr string) (res []PutPayRequest) {
	req := AllRequest{Key: "Address", Value: addr}
	jsonBytes, err := json.Marshal(req)
	if err != nil {
		return nil
	}

	jsonData, err := GetJsonData("/PayRequest.all", string(jsonBytes), "js")
	if err != nil {
		return nil
	}
	jsonVal := Parse(jsonData)

	for _, row := range jsonVal.Rows {
		val1, ok1 := row["OptAmount"].(float64)
		if !ok1 {
			continue
		}
		val2, ok2 := row["CreatedAt"].(float64)
		if !ok2 {
			continue
		}
		val3, ok3 := row["OptExpireAt"].(float64)
		if !ok3 {
			continue
		}
		res = append(res, PutPayRequest{
			OptAmount:   uint64(val1),
			CreatedAt:   uint64(val2),
			OptExpireAt: uint64(val3),
		})
	}

	return res
}
