package main

func (a *AppReceive) ViewStealth() {

	var strings []string
	for k := range stealthbalances {
		strings = append(strings, k)
	}
	sortStrings(strings)

	a.stealths.SetInnerHTML("")
	for _, k := range strings {
		v := stealthbalances[k]
		if v > 0 {
			AppendChild(a.stealths, "tr", row(k, v))
		}
	}
	for _, k := range strings {
		v := stealthbalances[k]
		if v == 0 {
			AppendChild(a.stealths, "tr", row(k, v))
		}
	}

}
func (a *AppReceive) ViewRequests(addr string) {

	a.requests.SetInnerHTML("")
	for v := range stealthrequests[addr] {
		AppendChild(a.requests, "tr", row3(v[0], v[1], v[2]))
	}

}
func (a *AppSend) ViewRequests(addr string) {

	a.requests.SetInnerHTML("")
	for v := range stealthrequests[addr] {
		AppendChild(a.requests, "tr", row3(v[0], v[1], v[2]))
	}

}
