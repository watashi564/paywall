package main

import "strconv"
import "strings"

func (a *App) bindEvents() {
	a.menu.bindEvents(a.main, a)
	a.send.bindEvents()
	a.recv.bindEvents()
}

func (a *AppMain) bindEvents(s *AppMain, app *App) {

	AddEventListener(a.send, "click", "target", func(this, target JQuery) {
		Show(s.send)
		Hide(s.recv)

		var val = Value(a.addr)

		app.send.globalAddress = val
		go app.send.getPayRequests(val, true)
	})

	AddEventListener(a.recv, "click", "target", func(this, target JQuery) {
		Hide(s.send)
		Show(s.recv)

		var val = Value(a.addr)

		app.recv.globalAddress = val

		app.recv.generateStealth(val, 0, false)
		go app.recv.generateStealth(val, 0, true)
	})
}
func (a *AppSend) bindEvents() {
	AddEventListener(a.upload, "click", "target", func(this, target JQuery) {

		var history = Value(a.history)

		// telegram fix
		history = strings.ReplaceAll(history, " ", "\n")
		// windows newlines
		history = strings.ReplaceAll(history, "\r\n", "\n")
		// done
		var lines = strings.Split(history, "\n")
		for _, line := range lines {
			line = strings.TrimSpace(line)
			if len(line) != 156 && len(line) != 1481 && len(line) != 1421 {
				continue
			}
			go PayHistory(&PutPayHistory{
				History: line,
				Address: a.globalAddress,
			})
			//Alert(line)
		}
	})
}
func (a *AppReceive) bindEvents() {

	AddEventListener(a.stealths, "click", "target", func(this, target JQuery) {
		var str = target.InnerHTML()
		if len(str) != 64 {
			return
		}

		var str2 = bech32get(commitment(str))
		SetSrc(a.image, QRCODE(str))
		SetSrc(a.image2, QRCODE("bitcoin:"+str2+"?amount=0.00000330"))

		SetValue(a.key, str)
		SetValue(a.keyclaim, str2)

		Show(a.keyclicked)

		go a.getPayRequests(str)
	})

	AddEventListener(a.request, "click", "target", func(this, target JQuery) {
		var stealthAddr = Value(a.key)
		var apiKey = a.getApiKey(stealthAddr)
		if len(apiKey) == 0 {
			return
		}

		amt, _ := strconv.ParseUint(Value(a.amount), 0, 64)
		cre, _ := strconv.ParseUint(Value(a.created), 0, 64)
		exp, _ := strconv.ParseUint(Value(a.expires), 0, 64)

		go func() {

			if PayRequest(&PutPayRequest{
				ApiKey:      apiKey,
				Address:     stealthAddr,
				OptAmount:   amt,
				CreatedAt:   cre,
				OptExpireAt: exp,
			}) {
				go a.getPayRequests(stealthAddr)
			}
		}()
	})

	AddEventListener(a.download, "click", "target", func(this, target JQuery) {
		go func(addr string) {
			var apiKey = a.getApiKey(addr)
			var hist = GetHistories(addr, apiKey)

			var buffer = ""

			for h := range hist {
				buffer += h + "\r\n"
			}

			Download(buffer, addr+".dat")

		}(Value(a.key))

	})
}
