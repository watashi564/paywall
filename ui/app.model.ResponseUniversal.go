package main

type AppModelSuccess struct {
	Testnet bool `json:"Testnet"`
	Success bool `json:"Success"`
}
type AppModelResponseUniversal struct {
	AppModelSuccess
	AppModelResponseAll
}
