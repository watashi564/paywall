//go:build js
// +build js

package main

var stealthbalances = make(map[string]uint64)
var stealthrequests = make(map[string]map[[3]uint64]struct{})

func main() {
	app := NewApp()
	app.bindEvents()
	app.loadCss()
	select {}
}
