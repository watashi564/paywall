package main

import "encoding/json"

type AllRequest struct {
	ApiKey string
	Key    string
	Value  string
}

func CheckBalance(addr, backingAddr, stealthbuf string) uint64 {
	req := AllRequest{Key: "Address", Value: addr, ApiKey: backingAddr + stealthbuf + "0000000000000000"}
	jsonBytes, err := json.Marshal(req)
	if err != nil {
		return 0
	}

	jsonData, err := GetJsonData("/PayFinal.all", string(jsonBytes), "js")
	if err != nil {
		return 0
	}
	jsonVal := Parse(jsonData)
	var max uint64
	for _, row := range jsonVal.Rows {
		val, ok := row["Amount"].(float64)
		if !ok {
			continue
		}
		if uint64(val) > max {
			max = uint64(val)
		}
	}

	return max
}
