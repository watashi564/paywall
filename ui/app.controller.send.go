package main

import "time"

func (a *AppSend) getPayRequests(addr string, filter bool) {
	var reqs = GetPayRequests(addr)
	for _, v := range reqs {

		if filter {
			// filter only
			now := uint64(time.Now().Unix())
			if v.OptExpireAt != 0 && v.OptExpireAt < now {
				continue
			}
			if v.CreatedAt > now {
				continue
			}
		}
		if stealthrequests[addr] == nil {
			stealthrequests[addr] = make(map[[3]uint64]struct{})
		}
		stealthrequests[addr][[3]uint64{v.OptAmount, v.OptExpireAt, v.CreatedAt}] = struct{}{}
	}

	a.ViewRequests(addr)

}
