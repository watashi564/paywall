package main

import "encoding/json"
import "encoding/base64"
import "net/http"
import "strconv"

func controller_crud(entity, action, payload string, w http.ResponseWriter, r *http.Request) {

	jsonData, err := base64.StdEncoding.DecodeString(payload)
	if err != nil {
		_, _ = w.Write(FAIL)
		return
	}

	var code = entityCode[entity]
	if code == 0 {
		_, _ = w.Write(FAIL)
		return
	}

	if action == "all" {
		controller_all(code, jsonData, w, r)
		return
	}

	var eType = entityType[code]
	err = json.Unmarshal(jsonData, eType)
	if err != nil {
		_, _ = w.Write(FAIL)
		return
	}
	var eValidator = entityValid[code]
	if !eValidator.Validate() {
		_, _ = w.Write(FAIL)
		return
	}

	switch action {
	case "get":
		controller_get(code, eType, w, r)
	case "put":
		controller_put(code, eType, w, r)
	case "del":
		controller_del(code, eType, w, r)
	}
}

func controller_get[T any](code byte, entity T, w http.ResponseWriter, r *http.Request) {
	byteSlice, err := json.Marshal(entity)
	if err != nil {
		_, _ = w.Write(FAIL)
		return
	}
	if PaywallLvlDbExistsJson(code, byteSlice) {
		_, _ = w.Write(FOUND)
		return
	}
	_, _ = w.Write(NFOUND)
}

func controller_put[T any](code byte, entity T, w http.ResponseWriter, r *http.Request) {
	byteSlice, err := json.Marshal(entity)
	if err != nil {
		_, _ = w.Write(FAIL)
		return
	}
	var idx map[string]interface{}
	err = json.Unmarshal(byteSlice, &idx)
	if err != nil {
		_, _ = w.Write(FAIL)
		return
	}
	PaywallLvlDbNewBatch()
	PaywallLvlDbPutJson(code, byteSlice)
	PaywallLvlDbPutIndex(code, byteSlice, idx)
	PaywallLvlDbWriteBatch()
	_, _ = w.Write(SUCC)
	return
}

func controller_del[T any](code byte, entity T, w http.ResponseWriter, r *http.Request) {
	byteSlice, err := json.Marshal(entity)
	if err != nil {
		_, _ = w.Write(FAIL)
		return
	}
	var idx map[string]interface{}
	err = json.Unmarshal(byteSlice, &idx)
	if err != nil {
		_, _ = w.Write(FAIL)
		return
	}
	PaywallLvlDbNewBatch()
	PaywallLvlDbDelJson(code, byteSlice)
	PaywallLvlDbDelIndex(code, byteSlice, idx)
	PaywallLvlDbWriteBatch()
	_, _ = w.Write(SUCC)
	return
}

func controller_all(code byte, jsonData []byte, w http.ResponseWriter, r *http.Request) {
	var e AllEntity
	err := json.Unmarshal(jsonData, &e)
	if err != nil {
		_, _ = w.Write(FAIL)
		return
	}

	if !policy_all(code, e) {
		_, _ = w.Write(FAIL)
		return
	}

	var seen bool
	_, _ = w.Write([]byte(`{"Success":true,"Rows":[`))

	function := PaywallLvlDbAll
	if e.Unique {
		function = PaywallLvlDbUnique
	}

	allCount := function(PaywallPrefix(e.Key, e.Value, code), code, e.Limit, func(val []byte) {
		if seen {
			_, _ = w.Write([]byte(`,`))
		}
		_, _ = w.Write(val)
		seen = true
	})
	_, _ = w.Write([]byte(`],"AllCount":` + strconv.Itoa(allCount) + `}`))
}
