package main

import (
	"encoding/binary"
	"github.com/spaolacci/murmur3"
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/util"
	"log"
)
import "crypto/sha256"
import "math"

func PaywallLvlDbPutJson(entityCode byte, json []byte) {
	var keyData = []byte{entityCode}
	var key = sha256.Sum256(json)
	keyData = append(keyData, (key[:])...)

	Paywallsbatch.Put(keyData, []byte(json))
}

func PaywallLvlDbHashIndex(v interface{}) ([32]byte, bool) {
	{
		vv, ok := v.(string)
		if ok {
			var val2 = sha256.Sum256([]byte((vv)))
			return val2, true
		}
	}
	{
		vv, ok := v.(float64)
		if ok {
			var buf [32]byte
			binary.LittleEndian.PutUint64(buf[24:], math.Float64bits(vv))
			return buf, true
		}
	}
	return [32]byte{}, false
}

func PaywallLvlDbPutIndex(entityCode byte, json []byte, indices map[string]interface{}) {
	var key = sha256.Sum256(json)

	for k, v := range indices {
		var val2, ok = PaywallLvlDbHashIndex(v)
		if !ok {
			println("Couldn't index ", k)
			continue
		}
		var val [8]byte
		binary.LittleEndian.PutUint64(val[:], murmur3.Sum64([]byte(k)))

		var keyData = []byte{255, entityCode}
		keyData = append(keyData, (val[:])...)
		keyData = append(keyData, (val2[:])...)
		keyData = append(keyData, (entityCode))
		keyData = append(keyData, (key[:])...)

		Paywallsbatch.Put(keyData, []byte{})
	}
}

func PaywallLvlDbExistsJson(entityCode byte, json []byte) bool {
	var keyData = []byte{entityCode}
	var key = sha256.Sum256(json)
	keyData = append(keyData, (key[:])...)

	var _, err2 = Paywallsdb.Get(keyData, nil)
	if err2 != nil {
		if err2 != leveldb.ErrNotFound {
			log.Fatal("Paywallsdb PaywallLvlDbExistsJson Error: ", err2)
		}
		return false
	}
	return true
}

func PaywallLvlDbDelJson(entityCode byte, json []byte) {
	var keyData = []byte{entityCode}
	var key = sha256.Sum256(json)
	keyData = append(keyData, (key[:])...)

	Paywallsbatch.Delete(keyData)
}
func PaywallLvlDbDelIndex(entityCode byte, json []byte, indices map[string]interface{}) {
	var key = sha256.Sum256(json)

	for k, v := range indices {
		var val2, ok = PaywallLvlDbHashIndex(v)
		if !ok {
			continue
		}
		var val [8]byte
		binary.LittleEndian.PutUint64(val[:], murmur3.Sum64([]byte(k)))

		var keyData = []byte{255, entityCode}
		keyData = append(keyData, (val[:])...)
		keyData = append(keyData, (val2[:])...)
		keyData = append(keyData, (entityCode))
		keyData = append(keyData, (key[:])...)

		Paywallsbatch.Delete(keyData)
	}
}

func PaywallPrefix(key, value string, entityCode byte) []byte {
	if len(key) == 0 {
		return nil
	}
	var keyData = []byte{255, entityCode}

	var val2 = sha256.Sum256([]byte((value)))
	var val [8]byte
	binary.LittleEndian.PutUint64(val[:], murmur3.Sum64([]byte(key)))

	keyData = append(keyData, (val[:])...)
	keyData = append(keyData, (val2[:])...)
	return keyData
}

func PaywallLvlDbAll(pref []byte, entityCode byte, limit *int, jsonCallback func([]byte)) (total int) {
	var prefx = append(pref, []byte{entityCode}...)

	iter := Paywallsdb.NewIterator(util.BytesPrefix(prefx), nil)
	for iter.Next() {
		var data []byte

		if len(prefx) > 1 {
			var err2 error
			key := iter.Key()[42:]
			data, err2 = Paywallsdb.Get(key, nil)
			if err2 != nil {
				if err2 != leveldb.ErrNotFound {
					log.Fatal("Paywallsdb PaywallLvlDbAll Error2: ", err2)
				}
				continue
			}
		} else {
			data = iter.Value()

		}

		total++
		if limit != nil {
			*limit--
			if *limit <= 0 {
				continue
			}
		}

		jsonCallback(data)
	}
	iter.Release()
	err := iter.Error()
	if err != nil {
		log.Fatal("Paywallsdb PaywallLvlDbAll Error: ", err)
	}
	return
}

func PaywallLvlDbUnique(pref []byte, entityCode byte, limit *int, jsonCallback func([]byte)) (total int) {
	if len(pref) > 10 {
		pref = pref[:10]
	}
	iter := Paywallsdb.NewIterator(util.BytesPrefix(pref), nil)
	var last = [42]byte{255, entityCode}
	for iter.Next() {
		key := iter.Key()
		if len(key) != 75 {
			continue
		}

		var this [42]byte
		copy(this[:], key)
		if this != last {

			last = this
			var data, err2 = Paywallsdb.Get(key[42:], nil)
			if err2 != nil {
				if err2 != leveldb.ErrNotFound {
					log.Fatal("Paywallsdb PaywallLvlDbAll Error2: ", err2)
				}
				continue
			}

			total++
			if limit != nil {
				*limit--
				if *limit <= 0 {
					continue
				}
			}

			jsonCallback(data)
		}
	}
	iter.Release()
	err := iter.Error()
	if err != nil {
		log.Fatal("Paywallsdb PaywallLvlDbAll Error: ", err)
	}
	return
}
