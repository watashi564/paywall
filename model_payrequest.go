package main

func init() {
	addEntityValidable("PayRequest", &PayRequest{})
}

type PayRequest struct {
	Address     string
	OptAmount   uint64
	OptExpireAt uint64
	CreatedAt   uint64
}

func (p *PayRequest) AddressIsHex() bool {
	for i := range p.Address {
		if (p.Address[i] >= '0' && p.Address[i] <= '9') ||
			(p.Address[i] >= 'A' && p.Address[i] <= 'F') ||
			(p.Address[i] >= 'a' && p.Address[i] <= 'f') {
			continue
		} else {
			return false
		}
	}
	return true
}

func (p *PayRequest) Validate() bool {
	if p.OptAmount >= 9007199254740993 {
		return false
	}
	if p.OptExpireAt >= 9007199254740993 {
		return false
	}
	if p.CreatedAt >= 9007199254740993 {
		return false
	}
	if !p.AddressIsHex() {
		return false
	}
	return len(p.Address) == 64
}
