package main

import "strings"
import "fmt"
import "crypto/sha256"
import "encoding/hex"

func init() {
	addEntityValidablePolicyable("PayHistory", &PayHistory{})
}

type PayHistory struct {
	Address   string
	History   string
	CreatedAt uint64
}

func (p *PayHistory) AddressIsHex() bool {
	for i := range p.Address {
		if (p.Address[i] >= '0' && p.Address[i] <= '9') ||
			(p.Address[i] >= 'A' && p.Address[i] <= 'F') ||
			(p.Address[i] >= 'a' && p.Address[i] <= 'f') {
			continue
		} else {
			return false
		}
	}
	return true
}
func (p *PayHistory) EndsHex(n int) bool {
	for i := n; i < len(p.History); i++ {
		if (p.History[i] >= '0' && p.History[i] <= '9') || (p.History[i] >= 'A' && p.History[i] <= 'F') {
			continue
		} else {
			return false
		}
	}
	return true
}
func (p *PayHistory) IsStack() bool {
	return (strings.HasPrefix(p.History, `\stack\data\`) ||
		strings.HasPrefix(p.History, `/stack/data/`)) &&
		len(p.History) == 12+72*2 && p.EndsHex(12)
}

func (p *PayHistory) IsTx() bool {
	return (strings.HasPrefix(p.History, `\tx\recv\`) ||
		strings.HasPrefix(p.History, `/tx/recv/`)) &&
		len(p.History) == 9+23*32*2 && p.EndsHex(9)
}

func (p *PayHistory) IsMerkleTx() bool {
	return (strings.HasPrefix(p.History, `\merkle\data\`) ||
		strings.HasPrefix(p.History, `/merkle/data/`)) &&
		len(p.History) == 13+22*32*2 && p.EndsHex(13)
}

func (p *PayHistory) IsHistory() bool {
	return p.IsStack() || p.IsTx() || p.IsMerkleTx()
}

func (p *PayHistory) Validate() bool {
	if p.CreatedAt >= 9007199254740993 {
		return false
	}
	if !p.AddressIsHex() {
		return false
	}
	if !p.IsHistory() {
		return false
	}
	return len(p.Address) == 64
}

func (p *PayHistory) Policy(apiKey, key, value string) bool {
	{
		decoded, err := hex.DecodeString(apiKey)
		if err != nil {
			return false
		}
		sum := sha256.Sum256(decoded)
		val := fmt.Sprintf("%X", sum)
		if val == value {
			return true
		}
	}
	{
		decoded, err := hex.DecodeString(TestnetWhitepaper + TestnetWhitepaper + apiKey)
		if err != nil {
			return false
		}
		sum := sha256.Sum256(decoded)
		val := fmt.Sprintf("%x", sum)
		if val == value {
			return true
		}
	}
	return false
}
