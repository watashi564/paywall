package main

import (
	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/filter"
	"github.com/syndtr/goleveldb/leveldb/opt"
	"log"
	"sync"
)

const PAYWALLS_LVLDB_PATH = "paywalls"

var Paywallsdb *leveldb.DB
var Paywallsbatch *leveldb.Batch
var PaywallsMutex sync.Mutex

func PaywallLvlDbOpen() {
	path := PAYWALLS_LVLDB_PATH
	db, err := leveldb.OpenFile(path, &opt.Options{
		Compression: opt.NoCompression,
		Filter:      filter.NewBloomFilter(10),
	})
	if err != nil {
		log.Fatal("DB open error: ", err)
	}
	Paywallsdb = db
}

func PaywallLvlDbClose() {
	if Paywallsdb == nil {
		return
	}
	Paywallsdb.Close()
	Paywallsdb = nil
}

func PaywallLvlDbNewBatch() {
	PaywallsMutex.Lock()
	Paywallsbatch = new(leveldb.Batch)
}

func PaywallLvlDbWriteBatch() {
	err := Paywallsdb.Write(Paywallsbatch, &opt.WriteOptions{
		Sync: true,
	})
	if err != nil {
		log.Fatal("DB write batch error: ", err)
	}
	Paywallsbatch = nil
	PaywallsMutex.Unlock()
}
