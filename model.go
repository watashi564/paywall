package main

var entityType = make(map[byte]interface{})
var entityCode = make(map[string]byte)

// var entityCodeRev = make([]string, 256, 256)
var entityValid = make(map[byte]Validator)
var entityPolicy = make(map[byte]Policyer)

func addEntity(name string, eType interface{}) {
	entityType[byte(1+len(entityCode))] = eType
	//entityCodeRev[byte(1+len(entityCode))] = name
	entityCode[name] = byte(1 + len(entityCode))

}
func addEntityValidable(name string, eType Validator) {
	entityType[byte(1+len(entityCode))] = (interface{})(eType)
	entityValid[byte(1+len(entityCode))] = eType
	//entityCodeRev[byte(1+len(entityCode))] = name
	entityCode[name] = byte(1 + len(entityCode))
}
func addEntityValidablePolicyable(name string, eType ValidatorPolicy) {
	entityType[byte(1+len(entityCode))] = (interface{})(eType)
	entityValid[byte(1+len(entityCode))] = Validator(eType)
	entityPolicy[byte(1+len(entityCode))] = eType
	//entityCodeRev[byte(1+len(entityCode))] = name
	entityCode[name] = byte(1 + len(entityCode))
}

type Validator interface {
	Validate() bool
}
type ValidatorPolicy interface {
	Validator
	Policyer
}
type Policyer interface {
	Policy(apiKey, key, value string) bool
}
type AllEntity struct {
	ApiKey string
	Key    string
	Value  string
	Limit  *int
	Unique bool
}
