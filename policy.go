package main

const TestnetWhitepaper = "2e3841b6e75e9717ab7d2a8b57248b7f611a5473381b5e432aaf8fe88874fbfe"

func policy_all(code byte, e AllEntity) bool {
	var policy = entityPolicy[code]
	if policy == nil {
		//fmt.Println("no policy for:", entityCodeRev[code], e.ApiKey, e.Key, e.Value)
		return true
	}
	return policy.Policy(e.ApiKey, e.Key, e.Value)
}
