#!/bin/bash

# this is a debug script for the paywall project. It sends 1 request to the be

echo "$1.$2.$3.js"

enc=$(echo -n "$3" | base64 -w 0  | tr '/+' '_-')

curl "http://127.0.0.1:21213/$1.$2.$enc.js"
echo ""
