package main

import "net/http"
import "embed"

//go:embed ui/index*.html
//go:embed ui/style*.css
//go:embed ui/wasm_exec*.js
//go:embed ui/paywall.wasm
var content embed.FS

var FAIL = []byte(`{"Success":false}`)
var SUCC = []byte(`{"Success":true}`)
var FOUND = []byte(`{"Success":true,"Found":true}`)
var NFOUND = []byte(`{"Success":true,"Found":false}`)

// cuturl cuts the url into up to 4 parts separated by dots after the last slash
func cuturl(path string) (paths []string) {
	const max_sep = 6
	var seps = make([]int, 1, max_sep)
	for i, c := range path {
		if c == '/' {
			seps = seps[0:1]
			seps[0] = i
		} else if c == '.' {
			seps = append(seps, i)
			if len(seps) == max_sep {
				break
			}
		}
	}
	for i := 1; i < len(seps); i++ {
		var min = 1 + seps[i-1]
		var max = seps[i]
		if min < max {
			paths = append(paths, path[min:max])
		} else {
			paths = append(paths, "")
		}
	}
	return
}

func controller_main_page_read(w http.ResponseWriter, r *http.Request) {
	w.Header().Add("Content-Type", "text/html")

	_, _ = w.Write([]byte(`<meta http-equiv="refresh" content="0; url=ui/index.html">`))
	http.Redirect(w, r, "ui/index.html", 301)
}

func controller_send_file(w http.ResponseWriter, name, ctype string) {
	data, _ := content.ReadFile(name)
	w.Header().Set("Content-Type", ctype)
	_, _ = w.Write(data)
}

func public_paywall_protocol(w http.ResponseWriter, r *http.Request) {
	var paths = cuturl(r.URL.Path)

	w.Header().Set("Access-Control-Allow-Origin", "*")

	switch len(paths) {
	case 0:
		controller_main_page_read(w, r)
		return
	case 1: // get height call

		switch paths[0] {
		case "main":
			//main_page_json(w, r)
			return
		case "wasm_exec":
			controller_send_file(w, "ui/wasm_exec.js", "text/javascript")
			return
		case "wasm":
			controller_send_file(w, "ui/wasm.wasm", "application/wasm")
			return
		case "paywall":
			controller_send_file(w, "ui/paywall.wasm", "application/wasm")
			return
		case "index":
			controller_send_file(w, "ui/index.html", "text/html")
			return
		case "style":
			controller_send_file(w, "ui/style.css", "text/css")
			return
		default:
			//controller_getblock(paths[0], w, r)
			return
		}

	case 2: // get base range call

		switch paths[0] {
		case "wasm_exec":
			controller_send_file(w, "ui/wasm_exec."+paths[1]+".js", "text/javascript")
			return
		case "index":
			controller_send_file(w, "ui/index."+paths[1]+".html", "text/html")
			return
		case "style":
			controller_send_file(w, "ui/style."+paths[1]+".css", "text/css")
			return
		default:
			//controller_getrange(paths[0], paths[1], w, r)
			return
		}

	case 3: // get commits bloom call

		controller_crud(paths[0], paths[1], paths[2], w, r)

		//controller_getcommit(paths[0], paths[1], paths[2], "00000000", w, r)
		return

	case 4: // get commits prefix bloom call

		//controller_getcommit(paths[0], paths[1], paths[2], paths[3], w, r)
		return

	case 5: // get commits by verbatim prefixes bloom-less call
		//controller_getcommitsbyprefixes(paths[0], paths[1], paths[2], paths[3], paths[4], w, r)
		return

	default: // bug/unimplemented call
		return
	}
}
