package main

import (
	"log"
	"net"
	"net/http"
	"time"
)

func main() {
	// Setup Public Listen
	publn, err7 := net.Listen("tcp", "0.0.0.0:21213")
	if err7 != nil {
		log.Fatal(err7)
	}

	PaywallLvlDbOpen()

	pubr := http.HandlerFunc(public_paywall_protocol)

	pubsrv := &http.Server{
		Handler:        pubr,
		WriteTimeout:   60 * time.Second,
		ReadTimeout:    60 * time.Second,
		MaxHeaderBytes: 20000000,
	}
	err := pubsrv.Serve(publn)
	if err != nil {
		return
	}
}
