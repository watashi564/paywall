#!/bin/bash

# this is a debug script for the paywall project. It sends all requests to the be

./wire.sh PayHistory all "{}"


./wire.sh PayRequest put "{\"Address\":\"3000000000000000000000000000000000000000000000000000000000000003\"}"
./wire.sh PayRequest put "{\"Address\":\"4000000000000000000000000000000000000000000000000000000000000003\"}"
./wire.sh PayRequest put "{\"Address\":\"3000000000000000000000000000000000000000000000000000000000000004\"}"
./wire.sh PayRequest put "{\"Address\":\"3000000000000000000000000000000000000000000000000000000000000004\", \"OptAmount\":10}"
./wire.sh PayRequest all "{}"
./wire.sh PayRequest all "{\"Key\":\"Address\",\"Unique\":true}"
./wire.sh PayRequest all "{\"Key\":\"OptAmount\",\"Unique\":true}"
./wire.sh PayFinal put "{\"Address\":\"3000000000000000000000000000000000000000000000000000000000000003\", \"Amount\":100}"
./wire.sh PayFinal put "{\"Address\":\"8320F3BD3A3C68CEEC754805F242BA581E6FF3B4394B804ADF511CF29274423C\", \"Amount\":33}"
./wire.sh PayFinal put "{\"Address\":\"00F12045D1581657679F591514F5D076335AAE2693FE54B879A50E3D3978F257\", \"Amount\":34}"
./wire.sh PayFinal all "{\"Key\":\"Address\",\"Value\":\"3000000000000000000000000000000000000000000000000000000000000003\"}"
