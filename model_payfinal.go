package main

import "fmt"
import "crypto/sha256"
import "encoding/hex"

func init() {
	addEntityValidablePolicyable("PayFinal", &PayFinal{})
}

type PayFinal struct {
	Address   string
	Amount    uint64
	CreatedAt uint64
}

func (p *PayFinal) AddressIsHex() bool {
	for i := range p.Address {
		if (p.Address[i] >= '0' && p.Address[i] <= '9') ||
			(p.Address[i] >= 'A' && p.Address[i] <= 'F') ||
			(p.Address[i] >= 'a' && p.Address[i] <= 'f') {
			continue
		} else {
			return false
		}
	}
	return true
}

func (p *PayFinal) Validate() bool {
	if p.Amount >= 9007199254740993 {
		return false
	}
	if p.CreatedAt >= 9007199254740993 {
		return false
	}
	if !p.AddressIsHex() {
		return false
	}
	return len(p.Address) == 64
}

func (p *PayFinal) Policy(apiKey, key, value string) bool {
	{
		decoded, err := hex.DecodeString(apiKey)
		if err != nil {
			return false
		}
		sum := sha256.Sum256(decoded)
		val := fmt.Sprintf("%X", sum)
		if val == value {
			return true
		}
	}
	{
		decoded, err := hex.DecodeString(TestnetWhitepaper + TestnetWhitepaper + apiKey)
		if err != nil {
			return false
		}
		sum := sha256.Sum256(decoded)
		val := fmt.Sprintf("%x", sum)
		if val == value {
			return true
		}
	}
	return false
}
